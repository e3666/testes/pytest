import sys

from pytest import mark

from buzzfizz.main import jogo


@mark.number
def test_quando_jogo_receber_1_entao_deve_retornar_1():
    assert jogo(1) == 1


@mark.buzz
def test_quando_jogo_recebe_3_entao_retorna_buzz():
    assert jogo(3) == 'buzz'


@mark.buzz
def test_quando_jogo_recebe_6_entao_retorna_buzz():
    assert jogo(6) == 'buzz'


@mark.fizz
def test_quando_jogo_recebe_5_entao_retorna_fizz():
    assert jogo(5) == 'fizz'


@mark.buzz
@mark.fizz
def test_quando_jogo_recebe_15_entao_retorna_buzzfizz():
    assert jogo(15) == 'buzzfizz'


@mark.skip(reason='ainda não está pronto')
def test_que_quero_pular():
    assert jogo(99) == 'buzzfiiiiiiz'


# parametrize vai executar n testes sem eu precisar ficar passando o número toda hora
# ou seja, ele agrupa testes que serão iguais, mas com valores diferentes
@mark.parametrize('n', [3, 6, 9, 12])
def test_deve_retornar_buzz_para_multiplos_de_3(n):
    assert jogo(n) == 'buzz'


@mark.parametrize(
    'n, resultado_esperado',
    [(1, 1), (2, 2), (3, 'buzz'), (5, 'fizz'), (15, 'buzzfizz')],
)
def test_quando_entrar_n_o_resultado_sera_igaul_ao_esperado(
    n, resultado_esperado
):
    assert jogo(n) == resultado_esperado


# xfail é para quando você sabe que o teste vai falhar, tipo coisas que só rodam no linux, então o teste feito no windows vai falhar
# teste retorna false caso ele não falhe, então se eu rodar ele no linux e falhar, vai ser um erro
# mas se eu rodar no windows e funcionar, também será um erro
@mark.xfail(sys.platform == 'win32', reason="se o sistema for windows, então o teste vai falhar")
def test_quando_jogo_receber_65_deve_retornar_fizz():
    assert jogo(65) == 'fiz'


# skipif ele vai pular quando algo uma condição acontecer (não rodar se o sistema for windows)
@mark.skipif(sys.platform == 'win32', reason='não rodar para sistemas windows')
def test_quando_jogo_receber_55_deve_retornar_fizz():
    assert jogo(55) == 'fizz'

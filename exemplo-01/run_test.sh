#!/bin/bash

# rodar normal
pytest

# rodar com verbose
pytest -v

# rodar para parar de executar testes assim que falhar
pytest -x

# rodar com debuger
pytest --pdb

# rodar mostrando os prints
pytest -s

# rodar com filtro 
pytest -k filtro

# rodar com mark
pytest -m number

# rodar com mark e verbose
pytest -vm number

# rodar com tudo que não seja uma dada mark
pytest -vm "not number"

# rodar teste mostrando a resão de um teste ter sido skipado
pytest -vrs

# mostra o tempo de execução dos N testes mais lentos
# se N=0 então mostra o tempo de todos os testes
pytest durations=3 -vv
